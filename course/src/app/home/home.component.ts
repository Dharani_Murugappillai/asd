import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  logoimage: string='assets/image/logo.png';
  background: string='assets/image/bg.png';

  constructor() { }

  ngOnInit(): void {
  }

}
